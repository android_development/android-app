package ed.cardgames.gamemanager;

import ed.cardgames.cardset.Croupier;
import android.util.Log;

public class	GameManager
{

	private GameManagerResponse	response;
	private Croupier						croupier;

	public GameManager()
	{
		this.croupier = null;
		this.response = null;
	}

	public void croupierInit(int cmpCrdNum, int maxPlyCrd)
	{
		this.croupier = new Croupier(cmpCrdNum, maxPlyCrd);
		this.croupier.prepareDeck();		
	}
	
	public void responseInit()
	{
		this.response = new GameManagerResponse();
	}

	public GameManagerResponse getResponse(Integer cidx)
	{	
		this.response.clear();
		this.croupier.getGMResponse(this.response,cidx);

		return this.response;
	}

	public void newGame()
	{
		this.response.clear();
		this.croupier.cardRetake();
	}

	public void changeComparedCardsNumber(int numCards)
	{
		this.croupier.setComparedCardsNumber(numCards);
	}
}