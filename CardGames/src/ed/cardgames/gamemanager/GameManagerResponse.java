package ed.cardgames.gamemanager;

import java.util.List;
import java.util.ArrayList;

public class GameManagerResponse
{
	class Resource
	{
		private int	cardIndex;
		private int resource;
		
		public Resource(int cidx, int resId)
		{
			this.cardIndex = cidx;
			this.resource = resId;
		}

		public int getCardIndex()
		{
			return this.cardIndex;
		}

		public void setCardIndex(int cidx)
		{
			this.cardIndex = cidx;
		}

		public int getResource()
		{
			return this.resource;
		}

		public void setResource(int res)
		{
			this.resource = res;
		}
	}

  List<Resource> resources;
	int score;
	boolean doEndGame;

	public GameManagerResponse()
	{
		resources = new ArrayList<Resource>();
		score = 0;
		doEndGame = false;
	}

	public int getCardIndex(int idx)
	{
		return this.resources.get(idx).getCardIndex();
	}

	public int getResource(int idx)
	{
		return this.resources.get(idx).getResource();
	}

	public void setResponseResource(int cidx, int resId)
	{	
		this.resources.add(new Resource(cidx,resId));
	}

	public int getScore()
	{
		return this.score;
	}

	public void setScore(int sc)
	{
		this.score = sc;
	}

	public void doEndGame(boolean deg)
	{
		this.doEndGame = deg;
	}
	
	public boolean isEndGame()
	{
		return this.doEndGame;
	}

	public void clear()
	{
		if (!this.resources.isEmpty())
		{	
			this.resources.clear();
		}
		this.score = 0;
		this.doEndGame = false;
	}

	public boolean isResourcesEmpty()
	{
		boolean isEmpty = true;

		if (this.resources != null && !this.resources.isEmpty())
		{
			isEmpty = false;
		}
		
		return isEmpty;
	}
	
	public int resourcesSize()
	{
	 	int size = 0;
		if (this.resources != null)
		{
			size = this.resources.size();
		}
		return size;
	}
}