package ed.cardgames.cardset;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import android.util.Log;

import ed.cardgames.ReferenceCardResources;
import ed.cardgames.gamemanager.GameManagerResponse;
import ed.cardgames.cardset.deck.*;

public class Croupier
{
	private final String CARD_BACK = "cb";

	private ReferenceCardResources refCardRes;

	private Deck							deck;
	private List<PlayingCard>	openCardsSet;
	private int 							score;
	private int								comparedCardsNumber;
	private int 							openCardsTotalNumber;
	private int								maxPlayingCards;

	public Croupier(int ccn, int mpc)
	{
		this.score	= 0;
		this.openCardsTotalNumber = 0;
		this.comparedCardsNumber = ccn;
		this.maxPlayingCards = mpc;
		this.openCardsSet = new ArrayList<PlayingCard>();		
	}
	
	public void setComparedCardsNumber(int numCards)
	{
		if ( (1 < numCards) && (numCards < maxPlayingCards))
		{
			this.comparedCardsNumber = numCards;
		}
	}

	public void prepareDeck()
	{
		this.refCardRes = new ReferenceCardResources();
		this.deck = new Deck();
		this.deck.mixDeck();
	}
	
	public void cardRetake()
	{

		this.score	= 0;
		this.openCardsTotalNumber = 0;

		if (!this.openCardsSet.isEmpty())
		{
			this.openCardsSet.clear();
		}

		this.deck.mixDeck();
	}
	
	public void getGMResponse(GameManagerResponse gmResponse, Integer cidx)
	{
		PlayingCard curPlaingCard = null;

 		int resId = refCardRes.getResourceByCardKey(CARD_BACK);

		if (cidx != null) // if it`s not end game
		{
  		curPlaingCard = deck.getPlayingCardByIdx(cidx.intValue());

  		doPlay(curPlaingCard);

  		Iterator<PlayingCard> itr = openCardsSet.iterator();
  		while(itr.hasNext())
  		{	
  			PlayingCard pc = itr.next();

  			resId = refCardRes.getResourceByCardKey(pc.getCardKey());
  			if (!pc.isAvailable() || !pc.isFaceUp())
  			{
  				itr.remove();	// remove the card if not available or not open
  			}

  			if (!pc.isFaceUp())
  			{
  				resId = refCardRes.getResourceByCardKey(CARD_BACK);
  			}

  			gmResponse.setResponseResource(pc.getId(),resId);			
  		}

  		gmResponse.setScore(this.score);
  		
  		if (this.openCardsTotalNumber == this.maxPlayingCards)
  		{
  			gmResponse.doEndGame(true);
  		}
		}
		else
		{
			for(int i = 0; i < maxPlayingCards; i++)
			{
				curPlaingCard = this.deck.getPlayingCardByIdx(i);
				curPlaingCard.setFaceUp(false);
				curPlaingCard.setAvailable(true);
				gmResponse.setResponseResource(curPlaingCard.getId(),resId);
			}
			gmResponse.setScore(0);
			gmResponse.doEndGame(false);
		}
	}

	
	private void doPlay(PlayingCard curCard)
	{

		boolean equalsBySuit = true, equalsByNumber = true, doDisable = false, doClose = false;

		if (curCard.isAvailable())
		{
			if (!curCard.isFaceUp())	//if the card has closed
			{
				curCard.setFaceUp(true);
				openCardsSet.add(curCard);
		    this.score += -1;

				if (openCardsSet.size() > 1)
				{
					PlayingCard lastOpenedCard =this.openCardsSet.get(openCardsSet.lastIndexOf(curCard)-1);

					if (curCard.getSuit().equals(lastOpenedCard.getSuit()))
					{
						this.score += (1 * 4);
					}
					else
					{
						equalsBySuit = false;
					}
					
					if (curCard.getNumber().equals(lastOpenedCard.getNumber()))
					{
						this.score += (4 * 4);
					}
					else
					{
						equalsByNumber = false;
					}
					
					if ((equalsBySuit || equalsByNumber) == false)
					{
						doClose = true;
						this.score += (-2);
					}
					else
					{
						if (this.openCardsSet.size() == this.comparedCardsNumber)
						{
							doDisable = true;
							this.openCardsTotalNumber += this.comparedCardsNumber;
						}
					}
					
					if (doDisable || doClose)
					{
						for(PlayingCard pc: this.openCardsSet)
						{
	             if (doDisable)
									pc.setAvailable(false);

							 if (doClose)
									pc.setFaceUp(false);
						}
					}
				}
			}
			else
			{
				curCard.setFaceUp(false);
			}
		}		
	}
}