package ed.cardgames.cardset.deck.card;

public class Card
{
	public static enum CardNumber
	{
		TWO(2), THREE(3), FORE(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);

		private int 		number;
		private String  strNumber;

		CardNumber(int num)
		{
			this.number = num;
		}

		public int getNumber()
		{
			return number;
		}

		public String getStrNumber()
		{
 			String strNumber = "";

			switch(number)
			{
				case 11: strNumber = "j"; break;
				case 12: strNumber = "q"; break;
				case 13: strNumber = "k"; break;
				case 14: strNumber = "a"; break;
				default:
								strNumber = String.valueOf(number);
			}
			return strNumber;
		}
	}

	public static enum CardSuit
	{
		CLUBS("cl"), DIAMONDS("di"), HEARTS("he"), SPADES("sp");

		private String suit;

		CardSuit(String s)
		{
			this.suit = s;
		}

		public String getSuit(){
			return suit;
		}
	}

	private CardNumber  cardNumber;
	private CardSuit		cardSuit;

	public Card()
	{}

	public Card(CardNumber cn, CardSuit cs)
	{
		this.cardNumber = cn;
		this.cardSuit		= cs;
	}

	public CardNumber getNumber()
	{
		return this.cardNumber;
	}

	public void setNumber(CardNumber cn)
	{
		this.cardNumber = cn;
	}

	public CardSuit getSuit()
	{
		return this.cardSuit;
	}

	public void setSuit(CardSuit s)
	{
		this.cardSuit = s;
	}

	public String getCardKey()
	{	
		return cardSuit.getSuit() + cardNumber.getStrNumber();
	}
}