package ed.cardgames.cardset.deck;

import ed.cardgames.cardset.deck.card.Card;

public class PlayingCard extends Card
{
	private	int			id;
	private boolean isAvailable; // true is enabled, false is desable;
	private boolean isFaceUp;			 // true is face, false is cardback;
	
	public PlayingCard(Card.CardNumber cn, Card.CardSuit cs, int id)
	{
		super(cn,cs);
		this.id = id;
		this.isFaceUp = false;
		this.isAvailable = true;
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public boolean isFaceUp()
	{
		return this.isFaceUp;
	}

	public void setFaceUp(boolean fu)
	{
		this.isFaceUp = fu;
	}

	public boolean isAvailable()
	{
		return this.isAvailable;
	}

	public void setAvailable(boolean a)
	{
		this.isAvailable = a;
	}

	public String getRefKey(){
		return getCardKey();
	}

}