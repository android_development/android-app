package ed.cardgames.cardset.deck;

import java.util.List;
import java.util.ArrayList;
import android.util.Log;
import java.util.Random;
import ed.cardgames.cardset.deck.card.Card;

public class Deck
{
  private final int DECK_SIZE = 52;

	List<PlayingCard> deck;

	public Deck()
	{
		deck = new ArrayList<PlayingCard>();
		populateDeck();
	}

	public PlayingCard getPlayingCardByIdx(int cidx)
	{
		return deck.get(cidx);
	}

	private void populateDeck()
	{
		if (deck == null || deck.isEmpty())
		{			
			int idx = 0;
			for(Card.CardSuit cs: Card.CardSuit.values())
			{
				for(Card.CardNumber cn: Card.CardNumber.values())
					deck.add(new PlayingCard(cn,cs,idx++));
			}
		}
	}

	public void mixDeck()
	{
		PlayingCard tmpPlayingCard = null, curPlayingCard = null;
		
		if (deck != null && !deck.isEmpty())
		{
			int cidx = 0;
			Random rand = new Random();
			for (int i = 0; i < DECK_SIZE; i++)
			{
				  cidx = rand.nextInt(DECK_SIZE-i);
					curPlayingCard = deck.get(i);
					tmpPlayingCard = deck.get(cidx);
					curPlayingCard.setId(cidx);
					tmpPlayingCard.setId(i);
					deck.set(cidx,curPlayingCard);
					deck.set(i,tmpPlayingCard);
			}
		}
	}
}