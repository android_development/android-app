package ed.cardgames;

import java.util.HashMap;

public class ReferenceCardResources
{

	private HashMap<String,Integer> resourceMap;

	public ReferenceCardResources()
	{
		this.resourceMap = new HashMap<String,Integer>();
		populateReference();
	}	
	

	public int getResourceByCardKey(String key)
	{
		return resourceMap.get(key);
	}

	private void populateReference()
	{
		//CLUBS
		resourceMap.put("cl2",	R.drawable.cl2);
		resourceMap.put("cl3",	R.drawable.cl3);
		resourceMap.put("cl4",	R.drawable.cl4);
		resourceMap.put("cl5",	R.drawable.cl5);
		resourceMap.put("cl6",	R.drawable.cl6);
		resourceMap.put("cl7",	R.drawable.cl7);
		resourceMap.put("cl8",	R.drawable.cl8);
		resourceMap.put("cl9",	R.drawable.cl9);
		resourceMap.put("cl10",	R.drawable.cl10);
		resourceMap.put("clj",	R.drawable.clj);
		resourceMap.put("clq",	R.drawable.clq);
		resourceMap.put("clk",	R.drawable.clk);
		resourceMap.put("cla",	R.drawable.cla);

		//SPADES
		resourceMap.put("sp2",	R.drawable.sp2);
		resourceMap.put("sp3",	R.drawable.sp3);
		resourceMap.put("sp4",	R.drawable.sp4);
		resourceMap.put("sp5",	R.drawable.sp5);
		resourceMap.put("sp6",	R.drawable.sp6);
		resourceMap.put("sp7",	R.drawable.sp7);
		resourceMap.put("sp8",	R.drawable.sp8);
		resourceMap.put("sp9",	R.drawable.sp9);
		resourceMap.put("sp10",	R.drawable.sp10);
		resourceMap.put("spj",	R.drawable.spj);
		resourceMap.put("spq",	R.drawable.spq);
		resourceMap.put("spk",	R.drawable.spk);
		resourceMap.put("spa",	R.drawable.spa);

		//DIAMONDS
		resourceMap.put("di2",	R.drawable.di2);
		resourceMap.put("di3",	R.drawable.di3);
		resourceMap.put("di4",	R.drawable.di4);
		resourceMap.put("di5",	R.drawable.di5);
		resourceMap.put("di6",	R.drawable.di6);
		resourceMap.put("di7",	R.drawable.di7);
		resourceMap.put("di8",	R.drawable.di8);
		resourceMap.put("di9",	R.drawable.di9);
		resourceMap.put("di10",	R.drawable.di10);
		resourceMap.put("dij",	R.drawable.dij);
		resourceMap.put("diq",	R.drawable.diq);
		resourceMap.put("dik",	R.drawable.dik);
		resourceMap.put("dia",	R.drawable.dia);

		//HEARTS
		resourceMap.put("he2",	R.drawable.he2);
		resourceMap.put("he3",	R.drawable.he3);
		resourceMap.put("he4",	R.drawable.he4);
		resourceMap.put("he5",	R.drawable.he5);
		resourceMap.put("he6",	R.drawable.he6);
		resourceMap.put("he7",	R.drawable.he7);
		resourceMap.put("he8",	R.drawable.he8);
		resourceMap.put("he9",	R.drawable.he9);
		resourceMap.put("he10",	R.drawable.he10);
		resourceMap.put("hej",	R.drawable.hej);
		resourceMap.put("heq",	R.drawable.heq);
		resourceMap.put("hek",	R.drawable.hek);
		resourceMap.put("hea",	R.drawable.hea);

		resourceMap.put("cb",	R.drawable.cb); //cardback
	}

}