package ed.cardgames;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;

import android.view.View;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
	
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.app.Fragment;
import android.app.DialogFragment;

import android.content.res.Resources;

import ed.cardgames.gamemanager.*;

public class TwelveCards extends Activity implements AlertDialogFragment.AlertDialogListener
{
		private RelativeLayout	mainRelativeLayout, cardBoard;
		private DialogFragment	dialogFragment;
		private Menu						menu;
		private GameManager 		gameManager;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

				LayoutInflater layoutInflater = (LayoutInflater)this.getSystemService(this.LAYOUT_INFLATER_SERVICE);
				mainRelativeLayout = (RelativeLayout)layoutInflater.inflate(R.layout.main,null);
				cardBoard	= (RelativeLayout)mainRelativeLayout.findViewById(R.id.cardboard);

      	dialogFragment = new AlertDialogFragment();

				Resources res = getResources();
				int maxPlayingCards =  res.getInteger(R.integer.max_playing_cards);
				int defaultComparedCardsNumber = res.getInteger(R.integer.default_compared_cards_number);

				gameManager = new GameManager();
				gameManager.croupierInit(defaultComparedCardsNumber,maxPlayingCards);
				gameManager.responseInit();
			
				setContentView(R.layout.main);

    }

		public void changeView(View view)
		{
		  
			ImageView iv = (ImageView)cardBoard.findViewById(view.getId());

			GameManagerResponse gmResponse = gameManager.getResponse(cardBoard.indexOfChild(iv));
				
			updateView(gmResponse);

			if (gmResponse.isEndGame())
			{
				dialogFragment.show(getFragmentManager(),getResources().getString(R.string.end_game));
			}
		}		

		public void	selectMenuItem(MenuItem item)
		{
			String tagName = String.valueOf(item.getItemId());
			dialogFragment.show(getFragmentManager(),tagName);
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu)
		{
    	MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
			this.menu = menu;

    	return true;
		}

		@Override
    public void onDialogPositiveClick(DialogFragment dialog)
		{
			ImageView iv = null;
			int resId = 0;

			try
			{
				resId = Integer.valueOf(dialog.getTag());
			}catch(Exception e)
			{ 
				resId = 0;
			}

			switch(resId)
			{
        case R.id.two_cards:
				{
				 gameManager.changeComparedCardsNumber(getResources().getInteger(R.integer.compared_two_cards));
         swapEnabledMenuItem(R.id.two_cards,R.id.three_cards);
				}
				break;
	      case R.id.three_cards:
				{
				 gameManager.changeComparedCardsNumber(getResources().getInteger(R.integer.compared_three_cards));
         swapEnabledMenuItem(R.id.three_cards,R.id.two_cards);
				}
				break;
				default: break;				
			}

			GameManagerResponse gmResponse = gameManager.getResponse(null);
				
			updateView(gmResponse);

			gameManager.newGame();

    }

		private void updateView(GameManagerResponse response)
		{
		  ImageView iv = null;

			if (!response.isResourcesEmpty())
			{
				for (int i = 0; i < response.resourcesSize(); i++)
					{						
						iv = (ImageView)cardBoard.getChildAt(response.getCardIndex(i));
						iv.setImageResource(response.getResource(i));
					}
			}	

			TextView value = (TextView)findViewById(R.id.value);
			value.setText(String.valueOf(response.getScore()));		

			setContentView(mainRelativeLayout);
		}

		private void swapEnabledMenuItem(int item1, int item2)
		{
			MenuItem mi = this.menu.findItem(item1);
			mi.setEnabled(false);
			mi = this.menu.findItem(item2);
			mi.setEnabled(true);
		}
}

