package ed.cardgames;

import android.app.Activity;
import android.os.Bundle;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.AlertDialog;
import android.content.DialogInterface;

import android.util.Log;

class AlertDialogFragment extends DialogFragment {
		

		public interface AlertDialogListener
		{
    	public void onDialogPositiveClick(DialogFragment dialog);
    }

		private AlertDialogListener alertDialogListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
		{
		
    	// Use the Builder class for convenient dialog construction
      AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());			

			switch(Integer.valueOf(getTag()))
			{
        case R.id.new_game:
							{
					      builder.setTitle(R.string.title_new_game)
											 .setMessage(R.string.message_new_game);
							}
							break;
        case R.id.two_cards:
							{
					      builder.setTitle(R.string.title_switch_card)
											 .setMessage(R.string.message_2cards);
							}
							break;
        case R.id.three_cards:
							{
					      builder.setTitle(R.string.title_switch_card)
											 .setMessage(R.string.message_3cards);
							}
							break;
				default:
							{
								builder.setTitle(R.string.title_end_game)
											 .setMessage(R.string.message_game_over);
							}

			}

			builder.setPositiveButton(R.string.button_ok,
															  new DialogInterface.OnClickListener()
																	{
																		public void onClick(DialogInterface dialog, int id)
																		{
															      	alertDialogListener.onDialogPositiveClick(AlertDialogFragment.this);
																		}
																	}
																 )

						 .setNegativeButton(R.string.button_cancel,
																new DialogInterface.OnClickListener()
																	{
																		public void onClick(DialogInterface dialog, int id)
																		{}
																	}
															 );

      // Create the AlertDialog object and return it
      return builder.create();
    }

		@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            alertDialogListener = (AlertDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement AlertDialogListener");
        }
    }
}